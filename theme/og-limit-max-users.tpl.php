<?php
/**
 * @file
 *
 * Available variables:
 *  - $users Number of joined users
 *  - $max_users Number of join limit
 *
 *
 * @author Kálmán Hosszu - hosszu.kalman@gmail.com - http://www.kalman-hosszu.com
 */
?>
<div class="og_limit"><strong>Limit:</strong> <?php echo $users ?>/<?php echo $max_users ?></div>